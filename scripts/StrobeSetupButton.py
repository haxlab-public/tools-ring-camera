import serial
import time

# Open serial conenction to relay board
ser = serial.Serial('/dev/canakit',115200, timeout=1)
ser.write('\n\r')
val = ser.read(size=64)
print (val)

# Establish a low setting for relay
print("Setup button low...")
ser.write('REL1.OFF\n\r')
val = ser.read(size=64)
print (val)
time.sleep(1)

# Press setup button
print("Setup button high...")
ser.write('REL1.ON\n\r')
val = ser.read(size=64)
print (val)
time.sleep(1)

# Release setup button
print("Restoring setup button low...")
ser.write('REL1.OFF\n\r')
val = ser.read(size=64)
print (val)

ser.close()

